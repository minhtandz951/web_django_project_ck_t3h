from django.contrib.auth.models import User

from django import forms

class RegistrationForm(forms.ModelForm):
    
    password = forms.CharField(widget=forms.PasswordInput(attrs={
        'placeholder' :'Enter Password',
    }))
    
    confirm_password = forms.CharField(widget=forms.PasswordInput(attrs={
        'placeholder' :'Confirm Password',
    }))
    
    class Meta:
        model = User
        fields = ['username', 'email', 'password']
        
    def __init__(self, *args, **kwargs):
        super(RegistrationForm, self).__init__( *args, **kwargs)
        self.fields['username'].widget.attrs['placeholder'] = 'User Name'
        self.fields['email'].widget.attrs['placeholder'] = 'Email Address'
         
        for field in self.fields:
            self.fields[field].widget.attrs['class'] = 'form-control'
                    
    def clean_username(self):
        cleaned_data = super(RegistrationForm, self).clean()
        username = cleaned_data.get('username')
        try:
            User.objects.get(username=username)
        except User.DoesNotExist:
            return username
        raise forms.ValidationError("Tài khoản đã tồn tại")
    
    def clean_email(self):
        cleaned_data = super(RegistrationForm, self).clean()
        email = cleaned_data.get('email')
        try:
            User.objects.get(email = email)
        except User.DoesNotExist:
            return email
        raise forms.ValidationError("Email đã được đăng kí!")
            
    def clean(self):
        cleaned_data = super(RegistrationForm, self).clean()
        password = cleaned_data.get('password')
        confirm_password = cleaned_data.get('confirm_password')
        
        if password != confirm_password:
            raise forms.ValidationError(
                "Mật khẩu không hợp lệ!"
            )
     
    

            
        