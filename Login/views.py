from django.shortcuts import render, redirect
from django.http import HttpResponse
from .forms import RegistrationForm 
from django.contrib.auth.models import User
from user.models import UserProfile
from django.contrib import auth
from django.contrib import messages
import json
import requests

from cart.models import Cart, CartItem
from cart.views import _cart_id

#Verification email
from django.contrib.sites.shortcuts import get_current_site
from django.template.loader import render_to_string
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.utils.encoding import force_bytes
from django.core.mail import EmailMessage
from django.contrib.auth.decorators import login_required
from django.contrib.auth.tokens import default_token_generator
from django.conf import settings
# Create your views here.

def login(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
    
        user = auth.authenticate(request, username=username, password=password)
        if user is not None:
            try:
                cart = Cart.objects.get(cart_id = _cart_id(request))
                cart_items = CartItem.objects.filter(cart=cart)
                if cart_items.exists():
                    cart_items_user = CartItem.objects.filter(user = user) 
                    if cart_items_user.exists():   
                        for item in cart_items:
                            check = True
                            for item_user in cart_items_user:
                                if item.product.productId == item_user.product.productId:
                                    if item_user.variations == item.variations:
                                        item_user.quantity += item.quantity
                                        item_user.save()
                                        check = False
                                        break
                            if check:
                                item.user= user
                                item.save()
                    else:
                        for item in cart_items:
                            item.user = user
                            item.save()                    
            except:
                pass
            auth.login(request, user)
            url = request.META.get('HTTP_REFERER')
            try:
                query = requests.utils.urlparse(url).query 
                # next=/cart/checkout/
                params = dict(x.split('=') for x in query.split('&'))
                if 'next' in params:
                    nextPage = params['next']
                    return redirect(nextPage)
            except:
                return redirect('homepage')
            return redirect('homepage')
            
        else:
            messages.error(request, 'Incorrect username or password.')
            return redirect('login')
    return render(request, 'accounts/login.html')


def register(request):
    if request.method == 'POST':
        form = RegistrationForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            email = form.cleaned_data['email']
            password = form.cleaned_data['password']
            user = User.objects.create_user(username=username, email=email,password=password )
            user.is_active = 0
            user.save()
            UserProfile.objects.create(user=user)
            
            
            current_site = get_current_site(request)
            mail_subject = 'Activate your blog account.'
            message = render_to_string('accounts/account_verification_email.html', {
                'user': user,
                'domain': current_site,
                'uid': urlsafe_base64_encode(force_bytes(user.pk)),
                'token': default_token_generator.make_token(user)
            })
            send_email = EmailMessage(
                subject= mail_subject,
                body= message,
                from_email= settings.EMAIL_FROM_USER,
                to=[email])
            send_email.send()
            
            return redirect('/account/login/?command=verification&email='+email)
    else:
        form = RegistrationForm()
    context ={
        'form' : form,
    }
    return render(request, 'accounts/register.html', context)

def activate(request, uidb64, token):
    try:
        uid = urlsafe_base64_decode(uidb64).decode()
        user = User._default_manager.get(pk=uid)
    except(TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None
    if user is not None and default_token_generator.check_token(user, token):
        user.is_active = 1
        user.save()
        messages.success(request, 'Congratulations! Your account is activated')
    else:
        messages.error(request, 'Invalid activation link')
    return redirect('login')
        
def logout(request):
    auth.logout(request)
    return redirect('homepage')

def forgotPassword(request):
    if request.method == 'POST':
        email = request.POST.get('email', None)
        if User.objects.filter(email = email).exists():
            user = User.objects.get(email__exact = email)
            # Reset password email
            
            current_site = get_current_site(request)
            mail_subject = 'Reset your password.'
            message = render_to_string('accounts/reset_password_email.html', {
                'user': user,
                'domain': current_site,
                'uid': urlsafe_base64_encode(force_bytes(user.pk)),
                'token': default_token_generator.make_token(user)
            })
            send_email = EmailMessage(
                subject= mail_subject,
                body= message,
                from_email= settings.EMAIL_FROM_USER,
                to=[email])
            send_email.send()
            messages.success(request, 'Password reset email has been sent to your email address.')
            return redirect('login')
            
        else:
            messages.error(request, 'Account does not exists !')
            return redirect('forgotPassword')
    return  render(request, 'accounts/forgotPassword.html')



def resetpassword_validate(request, uidb64, token):
    try:
        uid = urlsafe_base64_decode(uidb64).decode()
        user = User._default_manager.get(pk=uid)
    except(TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None
    if user is not None and default_token_generator.check_token(user, token):
        request.session['uid'] = uid
        messages.success(request, 'Please Reset your password')
        return redirect('resetPassword')
    else:
        messages.error(request, 'This link has been expired')
        return redirect('login')
    
def resetPassword(request):
    if request.method == 'POST':
        password = request.POST['password']
        confirm_password = request.POST['confirm_password']
        
        if password == confirm_password:
            uid = request.session.get('uid')
            user = User.objects.get(pk=uid)
            user.set_password(password)
            user.save()
            
            messages.success(request, 'Password reset successful!')
            return redirect('login')
        else:
            messages.error(request,'Password do not match!')
            return redirect('resetPassword')
    else:
        return render(request, 'accounts/resetPassword.html')