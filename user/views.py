from django.shortcuts import render,redirect
from order.models import Order, OrderProduct
from user.models import UserProfile
from django.http import HttpResponse
import datetime
from django.contrib import messages
from django.contrib.auth.decorators import login_required
# Create your views here. 

@login_required(login_url='login')
def dashboard(request):
    
    userprofile = UserProfile.objects.get(user = request.user)
    user = UserProfile.objects.get(user = request.user)
    if request.method == 'POST':
        name = request.POST.get('name', None)
        phone = request.POST.get('phone', None)
        email = request.POST.get('email', None)
        sex = (request.POST.get('sex', None))
        day = request.POST.get('day', None)
        month = request.POST.get('month', None)
        year = request.POST.get('year', None)
        picture = request.FILES.get('picture_update', None)

        year = int(year)
        month = int(month)
        day= int(day)
        
        now = datetime.datetime(year=year, month=month, day=day)
        date = now.strftime('%Y-%m-%d')
        
        if sex !=None:
            sex = int(sex)
            userprofile.sex = sex
        if picture != None:
            userprofile.profile_picture = picture
        userprofile.name = name
        userprofile.phone = phone
        userprofile.day_of_birth = date
        userprofile.save()
        user.email = email
        messages.success(request, "Successfully updated personal information")
        
    
    orders = Order.objects.order_by('created_at').filter(user_id=request.user.id, is_ordered = True)
    order_count = orders.count()
    
    user_day_of_birth = str(userprofile.day_of_birth)
    user_day_of_birth = user_day_of_birth.split('-')
    user_day_of_birth[2] = int(user_day_of_birth[2])
    user_day_of_birth[1] = int(user_day_of_birth[1])
    user_day_of_birth[0] = int(user_day_of_birth[0])
    
    days = []
    for i in range(1,32):
        days.append(i)     
    months= []
    for i in range(1,13):
        months.append(i)
    years = []
    for i in range(1920, 2023):
        years.append(i)
        
    
    context = {
        'orders': orders,
        'order_count': order_count,
        'userprofile': userprofile,
        'days':days,
        'months':months,
        'years':years,
        'user_day_of_birth':user_day_of_birth,
    }
    return render(request, 'user/dashboard.html', context)

@login_required(login_url='login')
def change_address(request):
    userprofile = UserProfile.objects.get(user = request.user)
    
    if request.method == "POST":
        country = request.POST.get('country', None)
        city = request.POST.get('city', None)
        district = request.POST.get('district', None)
        state = request.POST.get('state', None)
        address = request.POST.get('address', None)
        
        userprofile.country = country
        userprofile.city = city
        userprofile.district = district
        userprofile.state = state
        userprofile.address = address
        userprofile.save()
        messages.success(request, "Successfully updated personal address")
        
    context = {
        'userprofile':userprofile
    }
    return render(request, 'user/change_address.html', context)

@login_required(login_url='login')
def my_orders(request):
    try:
        orders = Order.objects.filter(user = request.user, is_ordered = True)
    except:
        orders = None
        
    context = {
        'orders': orders
    }
    return render(request, 'user/my_orders.html', context)

@login_required(login_url='login')
def my_orders_detail(request, order_number=None):
    
    try:
        order = Order.objects.get(order_number = order_number, is_ordered = True)
        order_products = OrderProduct.objects.filter(order__id = order.id)
        total = 0
        for i in order_products:
            total += i.product_price * i.quantity
            i.variations = i.variations.replace("{", "")
            i.variations = i.variations.replace("}", "")
            i.variations = i.variations.replace("'", "")
            
        ship = (2 * total)/100
        grand_total = total + ship
    

        context = {
            'order': order,
            'order_number': order.order_number,
            'order_products': order_products,
            'total': total,
            'ship' : ship,
            'grand_total': grand_total,
            
        }
        return render(request, 'user/my_order_detail.html', context)
    except (Order.DoesNotExist):
        return redirect('home')
    