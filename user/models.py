from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class UserProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=100 ,blank =True)
    profile_picture = models.ImageField(blank=True, upload_to='upload/userprofile', default='upload/userprofile/user.jpg')
    phone = models.CharField(max_length=20, blank=True, null=True, default=None)
    sex = models.IntegerField(blank=True, default=None, null=True)
    day_of_birth = models.DateField(blank=True, default=None, null= True)
    country = models.CharField(max_length=50 ,blank =True)
    city = models.CharField(max_length=50 ,blank =True)
    district = models.CharField(max_length=50 ,blank =True)
    state = models.CharField(max_length=50 ,blank =True)
    address = models.CharField(blank=True, max_length = 100)
    
    def __str__(self) :
        return self.name

    def full_address(self):
        return "{0}, {1}, {2}, {3}, {4}".format(self.address, self.state, self.district, self.city, self.country)
    