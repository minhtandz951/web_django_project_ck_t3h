# Generated by Django 4.1.1 on 2022-12-11 17:21

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('user', '0003_userprofile_sex'),
    ]

    operations = [
        migrations.AddField(
            model_name='userprofile',
            name='phone',
            field=models.CharField(blank=True, default=None, max_length=20, null=True),
        ),
    ]
