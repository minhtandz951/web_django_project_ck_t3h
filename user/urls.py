from django.contrib import admin
from django.urls import path, include
from. import views


# from .views import index


urlpatterns = [
    path('',views.dashboard ,name ='dashboard'),
    path('/change_address/',views.change_address ,name ='change_address'),
    path('/my_orders/',views.my_orders ,name ='my_orders'),
    path('/my_order_detail/<slug:order_number>/',views.my_orders_detail ,name ='my_orders_detail'),
]
