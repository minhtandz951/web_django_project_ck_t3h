from django.contrib import admin
from .models import Payment, Order, OrderProduct
from django.utils.html import format_html

# Register your models here.


class OrderProductInline(admin.TabularInline):
    model = OrderProduct
    def thumbnail(self, object):
        return format_html('<img src="{}" width="50">'.format(object.product.image.url))
    
    thumbnail.short_description = 'Product image'
    readonly_fields = ('product','thumbnail','payment', 'user', 'product', 'quantity', 'product_price', 'ordered')
    extra = 0

class OrderAdmin(admin.ModelAdmin):
    list_display = ['order_number', 'name', 'phone', 'email', 'city', 'order_total', 'ship', 'status', 'is_ordered', 'created_at']
    list_filter = ['status', 'is_ordered']
    search_fields = ['order_number', 'first_name', 'last_name', 'phone', 'email']
    list_per_page = 20
    inlines = [OrderProductInline]

admin.site.register(Order, OrderAdmin)
admin.site.register(Payment)
admin.site.register(OrderProduct)