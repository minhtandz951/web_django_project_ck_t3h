from django.shortcuts import render, redirect, get_object_or_404
from cart.models import CartItem, Cart
from store.models import Product
from django.http import HttpResponse
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
import json
from django.contrib import messages

# Create your views here.

def cart(request):
    current_user = request.user
    cart_id = _cart_id(request)
    total = 0
    tax = 0
    total_all = 0
    if current_user.is_authenticated:
        cartItems = CartItem.objects.filter(user = current_user)
    else:
        cartItems = CartItem.objects.filter(cart__cart_id = cart_id)

    cart_count = cartItems.count()
    for cart_item in cartItems:
        total += cart_item.sub_total()
    tax = total * 2 /100
    total_all = total + tax
    
    for item in cartItems:
        item.variations = item.variations.replace("'", '')
        item.variations = item.variations.replace("{", '')
        item.variations = item.variations.replace("}", '')
    context ={
        'cart_items' :cartItems,
        'total': total,
        'tax': tax,
        'total_all': total_all,
        'cart_count': cart_count,
    }
    return render (request, 'carts/cart.html', context)

def _cart_id(request):
    cart = request.session.session_key
    if not cart:
        cart = request.session.create()
    return cart

def add_cart(request, product_id):
    current_user = request.user
    product = Product.objects.get(productId=product_id)
    #if the user is authenticated
    if current_user.is_authenticated:
        if request.method == 'POST':
            quantity = 1
            quantity = request.POST.get('quantity')
            color = request.POST.get('color', None)
            size = request.POST.get('size', None)
            
            variations = {}
            variations["color"] = color
            variations["size"] = size

            cart_item_check = CartItem.objects.filter(user = current_user, product = product)
            for item_check in cart_item_check:
                variations_check = item_check.variations.replace("'", '"')
                variations_check = json.loads(variations_check)
                if variations_check['color'] == color and variations_check['size'] == size:
                    item_check.quantity += int(quantity)
                    item_check.save()
                    return redirect('cart')

            cart_item = CartItem.objects.create(
                product = product,
                quantity = quantity,
                variations = variations,
                user = current_user,
            )
            return redirect('cart')
        #if the user is not authenticated
    else: 
        if request.method == 'POST':
            quantity = 1
            quantity = request.POST.get('quantity')
            color = request.POST.get('color', None)
            size = request.POST.get('size', None)
            
            variations = {}
            variations['color'] = color
            variations['size'] = size
            
            try:
                cart = Cart.objects.get(cart_id = _cart_id(request))
            except Cart.DoesNotExist:
                cart = Cart.objects.create(
                    cart_id = _cart_id(request)
                )
            cart.save()
            
            cart_item_check = CartItem.objects.filter(cart = cart, product = product)
            for item_check in cart_item_check:
                variations_check = item_check.variations.replace("'", '"')
                variations_check = json.loads(variations_check)
                if variations_check['color'] == color and variations_check['size'] == size:
                    item_check.quantity += int(quantity)
                    item_check.save()
                    return redirect('cart')
                
            cart_item = CartItem.objects.create(
                product = product,
                quantity = quantity,
                variations = variations,
                cart = cart,
            )
        return redirect('cart')


def increase_cart(request, cart_item_id):
    try:
        if request.user.is_authenticated:
            cart_item = CartItem.objects.get(user = request.user, id = cart_item_id)
        else:
            cart= Cart.objects.get(cart_id = _cart_id(request))
            cart_item = CartItem.objects.get(cart = cart, id = cart_item_id)
            

        cart_item.quantity +=1
        cart_item.save()
    except:
        pass
    return redirect('cart')

def decrease_cart(request, cart_item_id):
    try:
        if request.user.is_authenticated:
            cart_item = CartItem.objects.get(user = request.user, id = cart_item_id)
        else:
            cart= Cart.objects.get(cart_id = _cart_id(request))
            cart_item = CartItem.objects.get(cart = cart, id = cart_item_id)
               
        if cart_item.quantity > 1:
            cart_item.quantity -=1
            cart_item.save()
        else:
            cart_item.delete()
            messages.success(request, "Cart Item is has been deleted")
    except:
        pass
    return redirect('cart')


def delete_cart(request, cart_item_id):
    try:
        if request.user.is_authenticated:
            cart_item = CartItem.objects.get(user = request.user, id = cart_item_id)
        else:
            cart= Cart.objects.get(cart_id = _cart_id(request))
            cart_item = CartItem.objects.get(cart = cart, id = cart_item_id)
               
        cart_item.delete()
        messages.success(request, "Cart Item is has been deleted")
    except:
        pass
    return redirect('cart')



# Checkout
@login_required(login_url='login')
def checkout(request, total = 0, quantity=0, cart_items = None):
    try:
        ship = 0
        grand_total = 0
        user = User.objects.none()
        if request.user.is_authenticated:
            cart_items = CartItem.objects.filter(user = request.user, is_active=True)
            user = User.objects.get(id= request.user.id)
        else:  
            cart = Cart.objects.get(cart_id = _cart_id(request))
            cart_items = CartItem.objects.filter(cart=cart, is_active=True)
            
        for cart_item in cart_items:
            total += (cart_item.product.price * cart_item.quantity)
            quantity += cart_item.quantity
        ship = (2* total)/100
        grand_total = total+ship
        cart_count = cart_items.count()
    except ObjectDoesNotExist:
        pass
        
    context = {
        'total':total,
        'quantity':quantity,
        'cart_items':cart_items,   
        'ship':ship,
        'grand_total':grand_total,
        'user': user,
        'cart_count':cart_count,
    }
    return render(request, 'carts/checkout.html', context)
