from django.contrib import admin
from django.urls import path, include

from . import views

urlpatterns = [
    path('',views.cart,name ='cart'),
    path('add_cart/<int:product_id>',views.add_cart, name ='add_cart'),
    path('decrease_cart/<int:cart_item_id>',views.decrease_cart, name ='decrease_cart'),
    path('increase_cart/<int:cart_item_id>',views.increase_cart, name ='increase_cart'),
    path('delete_cart/<int:cart_item_id>',views.delete_cart, name ='delete_cart'),
    path('checkout', views.checkout, name='checkout'),
]
