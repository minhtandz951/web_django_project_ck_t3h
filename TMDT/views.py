from django.shortcuts import render, redirect, HttpResponseRedirect
from store.models import *
from django.views import View

def index(request):
    cart = request.session.get('cart')
    if not cart:
        request.session['cart'] = {}
    # if categoryId:
    #     products = Product.objects.filter(category = categoryId)
    # else:
    productfeatured = Product.objects.filter(price__range=(0, 150))
    productrecent = Product.objects.filter(price__range=(150, 1000))

    data = {}
    data['product_fe'] = productfeatured
    data['product_re'] = productrecent
    return render(request, 'home.html', data)
        