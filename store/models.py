from django.db import models
from django.urls import reverse
from django.contrib.auth.models import User
from django.db.models import Avg, Count

# Create your models here.

class Category(models.Model):
    categoryId = models.AutoField(primary_key = True)
    name = models.CharField(max_length = 100)
    code = models.SlugField(max_length = 100, unique = True)
    image = models.ImageField(upload_to = 'upload/categories')
    is_active = models.BooleanField(default= True)
    parents_id = models.IntegerField(blank=True, null= True, default = None)
    created_at =  models.DateTimeField(auto_now_add = True)
    
    def count_products_category(self):
        product_count = 0;
        if self.parents_id is None:
            categories = Category.objects.filter(parents_id = self.categoryId)
            for cate in categories:
                products = Product.objects.filter(category_id = cate.categoryId)
                product_count += products.count()  
        return product_count
    
    def get_url(self):
        return reverse('products_by_category', args = [self.code])
    
    @property 
    def imageURL(self):
        try:
            url = self.image.url 
        except:
            url = ''
        return url
    
    def __str__(self):
        return self.name
  
class Product(models.Model):
    productId = models.AutoField(primary_key = True)
    name = models.CharField(max_length = 100)
    code = models.SlugField(max_length = 100, unique = True)
    price = models.IntegerField(default = 0)
    variations = models.TextField(blank=True, null=True, default = None)
    category = models.ForeignKey(Category, on_delete = models.CASCADE, default = 1)
    description = models.TextField()
    image = models.ImageField(upload_to = 'upload/products')
    created_at =  models.DateTimeField(auto_now_add = True)
    
    def __str__(self):
        return self.name
    
    @property 
    def imageURL(self):
        try:
            url = self.image.url 
        except:
            url = ''
        return url
    
    def get_url(self):
        return reverse('product_detail', args = [self.category.code, self.code] )
    
    def averangeReview(self):
        reviews = ReviewRating.objects.filter(product = self, status = True).aggregate(average = Avg('rating'))
        avg = 0;
        if reviews['average'] is not None:
            avg = float(reviews['average'])
        return avg
    
    def countReview(self):
        reviews = ReviewRating.objects.filter(product = self, status = True).aggregate(count = Count('id'))
        count = 0
        if reviews['count'] is not None:
            count = int(reviews['count'])
        return count
    
class ReviewRating(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    subject = models.CharField(max_length= 100, blank=True )
    review = models.TextField(max_length= 500, blank = True)
    rating = models.FloatField()
    ip = models.CharField(max_length=20, blank=True)
    status = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)
    
    def __str__(self) :
        return self.subject
    
    
class ProductGallery(models.Model):
    product = models.ForeignKey(Product,default=None, blank=True, on_delete=models.CASCADE)
    image = models.ImageField(upload_to='upload/products', max_length= 255)
    
    def __str__(self):
        return self.product.name
    
    class Meta:
        verbose_name = 'productgallery'
        verbose_name_plural = 'product gallery'