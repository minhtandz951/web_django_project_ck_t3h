from django.contrib import admin
from django.urls import path, include

from store.view.store import index
from store.view.search import search
from store.view.reviews import Review

# from .views import index


urlpatterns = [
    path('',index.store,name ='store'),  
    path('category/<slug:category_code>/', index.store, name='products_by_category'),
    path('category/<slug:category_code>/<slug:product_code>/', index.product_detail, name='product_detail'),
    path('search/', search.search, name='search'),
    path('submit_review/<int:product_id>',Review.submit_review , name='submit_review'),
    
]
