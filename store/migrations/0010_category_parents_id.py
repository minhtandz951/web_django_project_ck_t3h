# Generated by Django 4.1.1 on 2022-11-17 06:24

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('store', '0009_alter_category_code_alter_product_code'),
    ]

    operations = [
        migrations.AddField(
            model_name='category',
            name='parents_id',
            field=models.IntegerField(default=None, null=True),
        ),
    ]
