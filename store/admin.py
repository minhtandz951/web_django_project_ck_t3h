from django.contrib import admin
from .models import *
import admin_thumbnails

@admin_thumbnails.thumbnail('image')
class ProductGalleryInline(admin.TabularInline):
    model = ProductGallery
    extra = 1
# Register your models here.
class ProductAdmin(admin.ModelAdmin):
    list_display = ('name', 'price',  'category',)
    prepopulated_fields = {'code':('name',)}
    inlines = [ProductGalleryInline]

class CategoryAdmin(admin.ModelAdmin):
    list_display = ('name','is_active',)
    prepopulated_fields = {'code':('name',)}
    
class ReviewAdmin(admin.ModelAdmin):
    list_display = ('subject','product','user',)
   
    list_per_page = 20
admin.site.register(Category, CategoryAdmin)
admin.site.register(Product, ProductAdmin)
admin.site.register(ReviewRating, ReviewAdmin)
admin.site.register(ProductGallery)


