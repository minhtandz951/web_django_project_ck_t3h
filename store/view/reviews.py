from django.shortcuts import render, redirect,get_object_or_404
from store.models import *
from django.views import View
from django.http import HttpResponse, JsonResponse
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
import json
from django.contrib import messages

class Review(View):
    def submit_review(request, product_id):
        url = request.META.get('HTTP_REFERER')
        if request.method == "POST":
            rating = float(request.POST.get('rating', None))
            review = request.POST.get('review', None)
            subject ='Too bad'
            if rating > 1:
                subject = 'Bad'
            if rating > 2:
                subject = 'Medium' 
            if rating > 3:
                subject = 'Pretty good' 
            if rating > 4:
                subject = 'Good'
            try:
                reviews = ReviewRating.objects.get(user__id = request.user.id, product_id = product_id)
                reviews.subject = subject
                reviews.rating = rating
                reviews.review = review
                reviews.save()
                messages.success(request, 'Thank you! Your review has been update')
                return redirect(url)
            except ReviewRating.DoesNotExist:
                reviews = ReviewRating()
                reviews.subject = subject
                reviews.rating = rating
                reviews.review = review
                reviews.ip = request.META.get('REMOTE_ADDR')
                reviews.product_id = product_id
                reviews.user_id = request.user.id
                reviews.save()
                messages.success(request, 'Thank you! Your review has been submitted')
        return redirect(url)