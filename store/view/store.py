from django.shortcuts import render, redirect,get_object_or_404
from store.models import *
from django.views import View
from order.models import Order, OrderProduct
from django.http import HttpResponse, JsonResponse
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
import json


class index(View):
    
    def store(request,category_code = None):
        
        categories = None
        products = Product.objects.all().order_by('productId')
        
        if request.method == "POST":
            if request.POST.get("price_all", None):
                products = Product.objects.all().order_by('productId')
                print(products)
            else:
                products = Product.objects.none()
                price_1 = request.POST.get("price_1", None)
                price_2 = request.POST.get("price_2", None)
                price_3 = request.POST.get("price_3", None)
                price_4 = request.POST.get("price_4", None)
                price_5 = request.POST.get("price_5", None)
                 
                if price_1 == 'on':
                    product = Product.objects.filter(price__range=(0, 100))
                    print(product)
                    for p in product:
                        products |= Product.objects.filter(pk = p.pk)
                
                if price_2 == 'on':
                    product = Product.objects.filter(price__range=(100, 200))
                    for p in product:
                        products |= Product.objects.filter(pk = p.pk)
                
                if price_3 == 'on':
                    product = Product.objects.filter(price__range=(200,300))
                    for p in product:
                        products |= Product.objects.filter(pk = p.pk)
                            
                if price_4 == 'on':
                    product = Product.objects.filter(price__range=(300,400))
                    for p in product:
                        products |= Product.objects.filter(pk = p.pk)
                            
                if price_5 == 'on':
                    product = Product.objects.filter(price__range=(400,100000))
                    for p in product:
                        products |= Product.objects.filter(pk = p.pk)       
                print(products)
            
            
            if request.POST.get("color_all", None):
                products = Product.objects.all().order_by('productId')
            elif request.POST.get("size_all", None):
                products = Product.objects.all().order_by('productId')
            else:  
                # products = Product.objects.none()
                color_red = request.POST.get("color_red", None)
                color_black = request.POST.get("color_black", None)
                color_white = request.POST.get("color_white", None)
                color_blue = request.POST.get("color_blue", None)
                color_yellow = request.POST.get("color_yellow", None)
                
                size_xs = request.POST.get("size_xs", None)
                size_s = request.POST.get("size_s", None)
                size_m = request.POST.get("size_m", None)
                size_l = request.POST.get("size_l", None)
                size_xl = request.POST.get("size_xl", None)
                
                
                product_all = Product.objects.all()
                for p in product_all:
                    if p.variations is not None:
                        variations = p.variations.replace("'", '"')
                        variations = json.loads(variations)
                        for key, item in variations.items():
                            if key == 'color':
                                item = item.split(',')
                                if color_red == 'on':
                                    if 'red' in item:
                                        if p not in products:
                                            products |= Product.objects.filter(pk = p.pk)
                                if color_black == 'on':
                                    if 'black' in item:
                                        if p not in products:
                                            products |= Product.objects.filter(pk = p.pk)
                                if color_blue == 'on':
                                    if 'blue' in item:
                                        if p not in products:
                                            products |= Product.objects.filter(pk = p.pk)
                                if color_white == 'on':
                                    if 'white' in item:
                                        if p not in products:
                                            products |= Product.objects.filter(pk = p.pk)
                                if color_yellow == 'on':
                                    if 'yellow' in item:
                                        if p not in products:
                                            products |= Product.objects.filter(pk = p.pk)
                            if key == 'size':
                                item = item.split(',')
                                if size_xs == 'on':
                                    if 'xs' in item:
                                        if p not in products:
                                            products |= Product.objects.filter(pk = p.pk)
                                if size_s == 'on':
                                    if 's' in item:
                                        if p not in products:
                                            products |= Product.objects.filter(pk = p.pk)
                                if size_m == 'on':
                                    if 'm' in item:
                                        if p not in products:
                                            products |= Product.objects.filter(pk = p.pk)
                                if size_l == 'on':
                                    if 'l' in item:
                                        if p not in products:
                                            products |= Product.objects.filter(pk = p.pk)
                                if size_xl == 'on':
                                    if 'xl' in item:
                                        if p not in products:
                                            products |= Product.objects.filter(pk = p.pk)
                
        else:
            if category_code != None:
                category = get_object_or_404(Category, code = category_code)
                if category.parents_id is None:
                    categories = Category.objects.filter(parents_id = category.categoryId)
                    products = Product.objects.none()
                    for cate in categories:
                        product = Product.objects.filter(category_id = cate.categoryId)
                        for p in product:
                            products |= Product.objects.filter(pk = p.pk)  
                else:     
                    products = Product.objects.filter(category = category)
                    
            else:
                products = Product.objects.all().order_by('productId')
                  
        product_object = Product.objects.all()
        product_count = product_object.count()
        product_count_price = {}
        product_count_price['0_0'] = Product.objects.all().count()
        product_count_price['0_1'] = Product.objects.filter(price__range=(0, 100)).count()
        product_count_price['1_2'] = Product.objects.filter(price__range=(100, 200)).count()
        product_count_price['2_3'] = Product.objects.filter(price__range=(200, 300)).count()
        product_count_price['3_4'] = Product.objects.filter(price__range=(300, 400)).count()
        product_count_price['4_5'] = Product.objects.filter(price__range=(400, 1000000)).count()
        
        product_count_color = {}
        product_count_color['red'] = 0
        product_count_color['white'] = 0
        product_count_color['black']= 0
        product_count_color['blue']= 0
        product_count_color['yellow'] = 0
        
        product_count_size = {}
        product_count_size['xs'] = 0
        product_count_size['s'] = 0
        product_count_size['m'] = 0
        product_count_size['l'] = 0
        product_count_size['xl'] = 0
        for product in product_object:
            if product.variations is not None:
                variations = product.variations.replace("'", '"')
                variations = json.loads(variations)
                for key, item in variations.items():
                    if key== 'color':
                        item = item.split(',')
                        if 'red' in item:
                            product_count_color['red'] =product_count_color['red']+1 
                        if 'blue' in item:
                            product_count_color['blue'] =product_count_color['blue']+1 
                        if 'black' in item:
                            product_count_color['black'] =product_count_color['black']+1 
                        if 'white' in item:
                            product_count_color['white'] =product_count_color['white']+1 
                        if 'yellow' in item:
                            product_count_color['yellow'] =product_count_color['yellow']+1 
                    if key== 'size':
                        item = item.split(',')
                        if 'xs' in item:
                            product_count_size['xs'] =product_count_size['xs']+1 
                        if 's' in item:
                            product_count_size['s'] =product_count_size['s']+1 
                        if 'm' in item:
                            product_count_size['m'] =product_count_size['m']+1 
                        if 'l' in item:
                            product_count_size['l'] =product_count_size['l']+1 
                        if 'xl' in item:
                            product_count_size['xl'] =product_count_size['xl']+1 
        
        
        print(products)
        paginator = Paginator(products, 9)
        page = request.GET.get('page')
        paged_products = paginator.get_page(page)    
        
        data = {
            'products' : paged_products,
            'product_count': product_count,
            'product_count_price': product_count_price,
            'product_count_color': product_count_color,
            'product_count_size' : product_count_size,

        }
        return render(request, 'store/store.html', data)
    
    
    def product_detail(request, category_code = None, product_code = None):
        try:
            single_product = Product.objects.get( category__code = category_code, code= product_code)
            
            dict_variations = {}
            if single_product.variations is not None:
                variations = json.loads(single_product.variations)
                for key, variation in variations.items():
                        arr =variation.split(",")
                        dict_variations[key] = arr      
        except Exception as e:
            raise e
        products =Product.objects.filter(category__code = category_code) 

        if request.user.is_authenticated:
            try:
                order_product = OrderProduct.objects.filter(user= request.user, product_id = single_product.productId).exists()
            except OrderProduct.DoesNotExist:
                order_product = None  
        else:
            order_product = None
            
        try:
            reviews = ReviewRating.objects.filter(product = single_product)
        except:
            reviews = None
            
        product_gallery = ProductGallery.objects.filter(product_id = single_product.productId)
        context = {
            'single_product' : single_product,
            'products' : products,
            'dict_variations':dict_variations,
            'order_product':order_product,
            'reviews':reviews,
            'product_gallery':product_gallery,

        }  
        return render(request, 'store/detail.html', context)
    
        