from django.shortcuts import render, redirect,get_object_or_404
from store.models import *
from django.views import View
from django.db.models import Q
from django.http import HttpResponse

class search(View):
    def search(request):
        if 'search' in request.GET:
            search = request.GET.get('search')
            products = Product.objects.order_by('created_at').filter(Q(name__icontains=search) | Q(description__icontains=search))
            product_count = products.count()
        context = {
            'products': products,
            'search': search,
            'product_count': product_count,
        }
        return render(request, 'store/store.html', context=context)