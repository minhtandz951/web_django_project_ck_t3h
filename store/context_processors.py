from store.models import Category

def menu_links(request):
    # links = Category.objects.all()
    links = Category.objects.filter(parents_id__isnull=True)
    links_category = Category.objects.filter(parents_id__isnull=False)
    return dict(links = links, links_cate = links_category)
